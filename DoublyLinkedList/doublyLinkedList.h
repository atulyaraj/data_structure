#ifndef DOUBLYLINKEDLIST_H_

#define DOUBLYLINKEDLIST_H_

typedef struct NodeD{
    struct NodeD *prev;
    int data;
    struct NodeD *next;
} NodeD;

extern NodeD *doublyLL;

void doublyLinkedListMenu();

void addElementDLL();

void addByIndexDLL();

void addElementMenuDLL();

void removeElementMenuDLL();

void removeByIndexDLL();

void removeLByValueDLL();

void displayDLL();

void doublyLinkedListDataStructure();

#endif