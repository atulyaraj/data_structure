#include<stdio.h>
#include<stdlib.h>

#include"doublyLinkedList.h"

NodeD *doublyLL;

void doublydoublyLLMenu(){

    printf("\t****DOUBLY LINKED LIST******\t\n");
    printf("\t* a. Add\t   \n");
    printf("\t* r. Remove\t   \n");
    printf("\t* v. View Linked List\t \n");
    printf("\t* 0. Close\t\t   \n");
    printf("\t****************************\n\n");
}

void addElementMenuDLL(){
        char addOption;
        do{
        printf("\t *********Add Element*****\n\n");
        printf("\t *(a): Add\n");
        printf("\t *(i): By Index\n");
        printf("\t *(0): Close\n\n");
        printf("\t * DoublyLinkedList>Add>");
            scanf(" %c", &addOption);

            switch(addOption){
                case 'a': addElementDLL(); break;
                case 'i': addByIndexDLL(); break;
                case '0': break;
                default: printf("\t Enter a valid option!!\n");                                
            }
        }while(addOption != '0');
}

void addElementDLL(){

    int inputData;

    NodeD *temp = (NodeD *)malloc(sizeof(NodeD));

    printf("\t* Add to Doubly Linked List \n");
    printf("\t* Enter element: ");
    scanf("%d", &inputData);

    temp->data = inputData;

    if(doublyLL == NULL){
        doublyLL = temp;
        temp->prev = temp->next = NULL;

    }else{
    
        NodeD *traverser = doublyLL;

        // Watchout!!
        while(traverser->next != NULL){
            traverser = traverser->next;
        }

        traverser->next = temp;
        temp->prev = traverser;
    }

}

void addByIndexDLL(){

    int inputData;
    int index;

    NodeD *temp = (NodeD *)malloc(sizeof(NodeD));

    printf("\t* Add to Doubly Linked List \n");
    printf("\t* Enter index: ");
    scanf("%d", &index);

    printf("\t* Enter element: ");
    scanf("%d", &inputData);

    temp->data = inputData;

    if(doublyLL == NULL){
        doublyLL = temp;
        temp->prev = NULL;
        temp->next = NULL; 
    }else{

        NodeD *p, *q;

        q = doublyLL;

        for(int i = 0; i < index - 1; i++){
            q = q->next;
            p = q->prev;
        }

        q->prev = temp;
        temp->next = q;

        temp->prev = p;
        p->next = temp;
    
    }

}

void removeElementMenuDLL(){
    char deleteOption;
        do{
        printf("\t *********Remove Element*****\n\n");
        printf("\t * i: By Index\n");
        printf("\t * v: By Value\n");
        printf("\t * 0: Close\n\n");
        printf("\t * DoublyLinkedList>Remove> ");
            scanf(" %c", &deleteOption);

            switch(deleteOption){
                case 'i': removeByIndexDLL(); break;
                case 'v': removeLByValueDLL(); break;
                case '0': break;
                default: printf("\t Enter a valid option!!\n");                                
            }
        }while(deleteOption != '0');

}

void removeByIndexDLL(){

    if(doublyLL == NULL){
        printf("\t* Nothing to remove!!\n\n");
        return;
    }else{
        NodeD *temp = doublyLL;
        doublyLL = temp->next;
        free(temp);
    }

}

void removeLByValueDLL(){

    if(doublyLL == NULL){
        printf("\t* Nothing to remove!!\n\n");
        return;
    }else{

        int value;
        printf("\t* Enter element: ");
        scanf("%d", &value);

        NodeD *p, *q;

        p = doublyLL;

        if(p->data == value){
            if(p->prev == NULL){
                // first element
                p = p->next;

                doublyLL = p;
                p->prev = NULL;
            }
        }else{

            q = p->prev;

            while(p->data != value){
                q = p->prev;


                printf("q holds %d\n", q->data);


                printf("p holds %d\n", p->data);

                p = p->next;
            }

            printf("after a while %d\n", p->data);


        }
    }
}

void displayDLL(){

    if(doublyLL == NULL){
        printf("\t* Empty Linked List!\n\n");
    }else{
        NodeD *val = doublyLL;

        printf("\t");

        while(val != NULL){
            printf("--> %d ",val->data);
            val = val->next;
        }
    }
    printf("\n\n");
}

void doublyLinkedListDataStructure(){
    char input;
    doublydoublyLLMenu();
    do{
    printf("\t*Double Linked List> Input: ");
    scanf(" %c", &input);
    
     switch(input){
        case 'a': addElementMenuDLL();  break;
        case 'r': removeElementMenuDLL(); break;
        case 'v': displayDLL(); break;
        case '0':break;
        default: printf("\t* Enter a valid option!!\n");                                
    }
    doublydoublyLLMenu();
    }while(input != '0'); 
}