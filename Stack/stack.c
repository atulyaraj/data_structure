#include<stdio.h>
#include<stdlib.h>
#include"stack.h"

int *stack;
int maxStack;
int top = -1;

void stackMenu(){

    printf("\t***********STACK************\t\n");
    printf("\t* a. Initialization\t   \n");
    printf("\t* p. Push\t   \n");
    printf("\t* o. Pop\t   \n");
    printf("\t* e. Peek\t   \n");
    printf("\t* v. View stack\t \n");
    printf("\t* f. Stack is full? \n");
    printf("\t* 0. Close\t\t   \n");
    printf("\t****************************\n\n");

}

void initializationS(){

    printf("\t* Stack>Initialization \t \n");
    printf("\t* Enter the size of stack: ");

    int sizeStack;

    scanf("%d", &sizeStack);

    maxStack = sizeStack - 1;

    stack = (int *)malloc(sizeof(int) * sizeStack);


    printf("\t****Stack has been initialized!\n\n");
        printf("\t * r. Redo\n");
        printf("\t * n. No\n");
        char initOption;
        printf("\t *Stack>Initialization>");
        
        scanf(" %c", &initOption);
        switch(initOption){
            case 'a': initializationS();
                        break;
            case '0': break;
        }
}

void push(){

    printf("\t*Stack>Push \n\n");
    printf("\t*Push: ");
    int data;
    scanf("%d", &data);
    if(top >= maxStack){
        printf("\t****Stack overflow!!\n\n");
        return;
    }else{
        top++;
        stack[top] = data;
        printf("\t* Pushed successfully!\n");

    }
}

void peek(){
    printf("\t*Stack>Peek \n\n");
    
    if(top == -1){
        printf("\t* Stack is empty.. \n\n");
        return;
    }
    else{
        printf("\t*==> %d\n\n", stack[top]);
    }
}

void pop(){

    printf("\t*Stack>Pop \n\n");
    printf("\t*Pop: ");

    if(top == -1){
        printf("\t****Stack underflow!!\n\n");
        return;
    }else{
        stack[top] = 0;
        top--;
        printf("\t* Poped successfully!\n\n");

        viewStack();
    }
}

void viewStack(){

    // for(int i = 0; i <= maxStack; i++){
    //     printf("%d \n",stack[i]);
    // }
    
    if(top == -1){
        printf("\t*==> null\n\n");
        return;
    }else{
        printf("\t* ==> %d\n", stack[top]);
        for(int i = top - 1; i >= 0; i--){
            printf("\t*     %d\n", stack[i]);
        }
    }
}

void isFull(){
    if(top == maxStack){
        printf("\t* Stack is full.\n");
    }else{
        printf("\t*No, %d more elements can be placed\n\n", maxStack - top);
    }
}


void stackData_structure(){

    stackMenu();

    char input;

 do{
    printf("\t* Stack>Input: ");
    scanf(" %c", &input);
    
     switch(input){
        case 'a': initializationS(); break;
        case 'p': push();  break;
        case 'o': pop(); break;
        case 'e': peek(); break;
        case 'v': viewStack(); break;
        case 'f': isFull(); break;
        case '0':break;
        default: printf("Enter a valid option!!\n");                                
    }

    stackMenu();


    }while(input != '0'); 


}
