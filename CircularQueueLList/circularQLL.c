#include<stdio.h>
#include<stdlib.h>
#include"circularQLL.h"

NodeCL *front = NULL, *rear = NULL;

void queueCLMenu(){

    printf("\t*Circular Queue Linked List*\t\n");
    printf("\t* e. Enqueue\t   \n");
    printf("\t* d. Dequeue\t   \n");
    printf("\t* v. View queue\t \n");
    printf("\t* h. Head\t   \n");
    printf("\t* t. Tail\t   \n");
    printf("\t* 0. Close\t\t   \n");
    printf("\t****************************\n\n");

}

void queueCLDataStructure(){
    char input;
    queueCLMenu();
    do{
    printf("\t* Circular Queue Linked List>Input: ");
    scanf(" %c", &input);
    
     switch(input){
        case 'e': enqueueCL();  break;
        case 'd': dequeueCL(); break;
        case 'v': viewCL(); break;
        case 'h': headCLElement(); break;
        case 't': tailCLElement(); break;
        case '0':break;
        default: printf("\t* Enter a valid option!!\n");                                
    }
    queueCLMenu();
    }while(input != '0'); 


}

void enqueueCL(){
    NodeCL *temp;
    temp = (NodeCL *)malloc(sizeof(NodeCL));

    printf("\t* Enter the element: ");
    int dataIn;
    scanf("%d",&dataIn);

    temp->data = dataIn;

    if(front == NULL){
        temp->next = temp;
        front = rear = temp;

    }else{
        rear->next = temp;
        temp->next = front;
        rear = temp;
    }
}

int dequeueCL(){

    NodeCL *q;

    if(front == NULL){
        printf("\t* Queue is empty!\n\n");
        return -1;
    }else if(front == rear){
        q = front;
        front = rear = NULL;
    }else{
        q = front;
        front = front->next;
        rear->next = front;
    }

    int data = q->data;
    printf("\t* Dequeued: %d\n\n", data);
    free(q);
    return data;
}

void viewCL(){

    if(front == NULL){
        printf("\t* Queue is empty!\n\n");
        return;
    }
    NodeCL *q;
    q = front;
    do{
        printf("\t %d ", q->data);
        q = q->next;
    }while(q != front);

    printf("\n\n");


}

void headCLElement(){

}

void tailCLElement(){

}
