#ifndef CIRCULARQLL_H_
#define CIRCULARQLL_H_


typedef struct NodeCL{
    int data;
    struct NodeCL *next;
}NodeCL;

void queueCLMenu();

void queueCLDataStructure();

void headCLElement();

void tailCLElement();

void enqueueCL();

int dequeueCL();

void viewCL();



#endif