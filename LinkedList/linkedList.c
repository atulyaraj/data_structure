#include<stdio.h>
#include<stdlib.h>
#include "linkedList.h"


Node *linkedList = NULL;

int elements = 0;

void linkedListMenu(){
    printf("\t********LINKED LIST*********\t\n");
    printf("\t* a. Add\t   \n");
    printf("\t* r. Remove\t   \n");
    printf("\t* v. View Linked List\t \n");
    printf("\t* 0. Close\t\t   \n");
    printf("\t****************************\n\n");

}


void addElementMenuLL(){
        char addOption;
        do{
        printf("\t *********Add Element*****\n\n");
        printf("\t *(a): Add\n");
        printf("\t *(i): By Index\n");
        printf("\t *(v): By Value\n");
        printf("\t *(0): Close\n\n");
        printf("\t * LinkedList>Add>");
            scanf(" %c", &addOption);

            switch(addOption){
                case 'a': addLL(); break;
                case 'i': addByIndexLL(); break;
                case 'v': addByValueLL(); break;
                case '0': break;
                default: printf("\t Enter a valid option!!\n");                                
            }
        }while(addOption != '0');
}

void addLL(){
    Node *temp;

    temp = (Node *)malloc(sizeof(Node));

    printf("\t Enter value: ");
    int item;
    scanf("%d", &item);

    temp->data = item;

    if(linkedList == NULL){
        //First element
        linkedList = temp;
        temp->next =  NULL;

    }else{

        Node *traverser = linkedList;

        while(traverser->next != NULL){
            traverser = traverser->next;
        }

        traverser->next = temp;
        temp->next = NULL;

    }

    ++elements;
}

void addByIndexLL(){


    printf("\t Enter index: ");
    int index;
    scanf("%d", &index);

    if(--index <= elements){

        Node *temp;

        temp = (Node *)malloc(sizeof(Node));
    
        printf("\t Enter value: ");
        int item;
        scanf("%d", &item);

        temp->data = item;

        if(index == 0){
            //First element
            temp->next =  linkedList;
            linkedList = temp;
        }else{
            
            Node *traverser = linkedList;
            Node *prev = NULL;

            int i = 0;

            while(traverser->next != NULL && i < index){
                prev = traverser;
                traverser = traverser->next;
                i++;
            }

            if(prev == NULL){

                temp->next = traverser->next;
                traverser->next = temp;
            }else{
                prev->next = temp;
                temp->next = traverser;
            }
        }

        ++elements;

    }
}


void addByValueLL(){

    printf("Enter value: ");
    int value;
    scanf("%d", &value);

    Node * temp = (Node *)malloc(sizeof(Node));

    temp->data = value;

    if(linkedList == NULL || temp->data < linkedList->data){
        temp->next = linkedList;
        linkedList = temp;

    }else{

        Node *traverser = linkedList;

        while(traverser->next != NULL && traverser->next->data < temp->data){
            traverser = traverser->next;
        }

        temp->next = traverser->next;
        traverser->next = temp;
    }

    ++elements;
}


void removeElementMenuLL(){
    char deleteOption;
        do{
        printf("\t *********Remove Element*****\n\n");
        printf("\t *(r): Remove(Last)\n");
        printf("\t *(i): By Index\n");
        printf("\t *(v): By Value\n");
        printf("\t *(0): Close\n\n");
        printf("\t * LinkedList>Remove>");
            scanf(" %c", &deleteOption);

            switch(deleteOption){
                case 'r': removeLL(); break;
                case 'i':  break;
                case 'v': removeLByValue(); break;
                case '0': break;
                default: printf("\t Enter a valid option!!\n");                                
            }
        }while(deleteOption != '0');

}

void removeLL(){

    if(linkedList ==  NULL){
        printf("\t Nothing to remove!!\n\n");
    }

    if(linkedList->next == NULL){
        printf("\t Last element removed\n\n");
        linkedList = NULL;
    }else{

        Node *traverser = linkedList;
        Node *prev = NULL;

        while(traverser->next != NULL){
            prev = traverser;
            traverser = traverser->next;
        }

        prev->next = traverser->next;
        free(traverser);    
    }
    --elements;
}


void removeByIndexL(){

}

void removeLByValue(){

    printf("\t Enter value: ");
    int value;
    scanf("%d", &value);


    if(linkedList == NULL){
        printf("\t Nothing to remove\n\n");
    }else{
        if(value == linkedList->data){
            linkedList = linkedList->next;
        }else{
            Node *traverser = linkedList;
            Node *prev = NULL;

            while(traverser->data != value || traverser->next != NULL){
                prev = traverser;
                traverser = traverser->next;
            }

            if(traverser->next == NULL){
                printf("\t No element found!\n\n");
            }

            prev->next = traverser->next;


        }
    }

    --elements;
}



void display(){

    printf("\t List with %d elements\n\n", elements);

    if(linkedList == NULL){
        printf("\t* Empty Linked List!\n\n");
    }else{
        Node *val = linkedList;

        printf("\t");

        while(val != NULL){
            printf("--> %d ",val->data);
            val = val->next;
        }

    }

    printf("\n\n");
}

void linkedListDataStructure(){

    char input;

    linkedListMenu();

    do{
    printf("\t* Linked List> Input: ");
    scanf(" %c", &input);
    
     switch(input){
        case 'a': addElementMenuLL();  break;
        case 'r': removeElementMenuLL(); break;
        case 'v': display(); break;
        case '0':break;
        default: printf("\t* Enter a valid option!!\n");                                
    }
    linkedListMenu();

    }while(input != '0'); 


}