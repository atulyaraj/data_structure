#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_


typedef struct Node{
    int data;
    struct Node *next;
}Node;

extern  Node *linkedList;
extern int elements;

void linkedListMenu();

void addLL();

void addByIndexLL();

void addByValueLL();

void addElementMenuLL();

void removeElementMenuLL();

void removeLL();

void removeByIndexL();

void removeLByValue();

void display();

void linkedListDataStructure();

#endif