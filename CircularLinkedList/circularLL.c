#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
    int data;
    struct Node *next;
}Node;

Node *clist = NULL;

void add(int item){

    Node *temp;

    temp = (Node *)malloc(sizeof(Node));

    temp->data = item;

    if(clist == NULL){
        //First element
        clist = temp;
        temp->next =  clist;

    }else{

        Node *traverser = clist;

        // clist -->1 --> 2 --> 3--> 4 --> clist
        //                      tra  temp

        while(traverser->next != clist){
            traverser = traverser->next;
        }

        traverser->next = temp;
        temp->next = clist;
    }

}

// cl --> 1 --> 2 --> cl
//       tra

void display(){
    Node *traverser = clist->next;

    //cl --> 1 --> 2 --> 3 --> 4 --> cl
    //       tra
    printf("%p \n", clist);
    do{
        printf("%p data: %d \n", traverser, clist->data);
        clist = clist->next;
    }while(traverser != clist->next);
}

int main(){

    add(1);

    add(2);

    add(3);

     add(4);

    display();

    add(5);

    display(5);

}

