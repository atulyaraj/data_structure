#include<stdio.h>
#include<stdlib.h>
// #include "./Array/array.h"
// #include "./Stack/stack.h"
// #include "./Queue/queueMain.h"
// #include "./CircularQueueLList/circularQLL.h"
// #include "./LinkedList/linkedList.h"
 #include "./DoublyLinkedList/doublyLinkedList.h"



void mainMenu(){
        printf("\t************MENU************\t\n");
        printf("\t* 1. Array   \n");
        printf("\t* 2. Stack using Array  \n");
        printf("\t* 3. Queue using Array   \n");
        printf("\t* 4. Linked List   \n");
        printf("\t* 5. Stack using Linked List   \n");
        printf("\t* 6. Queue using Linked List   \n");
        printf("\t* 7. Circular Queue using Linked List   \n");
        printf("\t* 8. Doubly Linked List   \n");
        printf("\t* 0. Close   \n");
        printf("\t****************************\n");
}


struct node {
   int data;
   int key;
   struct node *next;
};
int main(){

    int userInput;
    char repeat;
    
    do{
        mainMenu();
        printf("\t* Input: ");
        scanf("%d", &userInput);

        switch(userInput){
            case 1: 
                //arrayData_structure();
                break;
            case 2: 
                //stackData_structure();
                break;

            case 3: 
                //queueMainDataStructure();
                break;
            case 4: 
                //linkedListDataStructure();
                break;
            case 5: break;
            case 6: break;
            case 7: 
                //queueCLDataStructure();
            
                    break;

            case 8: 
                doublyLinkedListDataStructure();

            break;
            
            case 0: exit(0);
            default: printf("Enter a valid option!!\n");                                
        }

        printf("Do you want continue? y/n: ");
        //fflush(stdin);
        scanf(" %c", &repeat);

    }while(repeat == 'y' || repeat == 'Y');

    return 0;
}
