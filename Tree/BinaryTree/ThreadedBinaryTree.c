/*
Drawback of binary tree;
A lot of NULL values in node. Node with no left child their left address feilds are 
empty and vice versa fo right child. And all the terminal nodes as they have no children
their left and right child address fields are empty therefore a lot of memory space is wasted.
To utilize those empty address fields TBT can be used. 

In TBT the empty address feilds keep the address of their upper nodes and therefore searching 
becomes fast. The pointers which point to the upper node are called threads and a binary tree
which implement those threads is called a threaded binary tree.


Threaded binary tree types: 
1. Pre
2. In
    a). Right In TBT
    b). Left In TBT
    c). Fully In TBT
    d). In TBT with header
3. Post

Right in TBT
Step 1: In order traversal
Step 2: Write in TBT, here the nodes which have there right address fields empty keep the address
        of their in-order sucessors.


Left in TBT:
Here the node which have their left address feilds empty keep the address of their in order predecessors

Fully In TBT:
Here of nodes have their left address feilds empty they keep address at in order predesscors. Right child 
is empty in order successors.

In TBT with header:

Header is of same type on Node

Head--left child -> Root node
Head--right child -> Head


if(Head--left == head){
    DS is empty
}


typedef enum{thread, link} bool;

typedef struct node{
    struct node *lptr;
    bool left;
    char data;
    bool right;
    struct node *rptr;
}node;


struct node* inSucc(struct node *ptr){
    struct node *succ;

    if(ptr->left == thread){
        pred = ptr->lptr;
    }else{
        pred = ptr->lptr;
        while(pred->right == link){
            pred = pred->rptr;
        }
    }
    return (pred);
}


void inorder(node *ptr){
    if(ptr->lptr == ptr){
        // Empty TBT
        return;
    }

    ptr = ptr->lptr;

    // Going to extreme left
    while(ptr->left != thread){
        ptr = ptr->lptr;
    }

    print(ptr->data);

    while(ptr->rptr != header){
       ptr =  inorderSuc(ptr);
       print(ptr->data);
    }

}


*/