/*
Tree traversals

Preorder
Inorder
Postorder

preorder(root){
    if(root == NULL){
        return
    }
    else{
        print(root->data)
        preorder(root->lchild)
        preorder(root->rchild)
    }
}

inorder(root){
    if(root == NULL){
        return
    }
    else{
        inorder(root->lchild)
        print(root->data)
        inorder(root->rchild)
    }
}

postorder(root){
    if(root == NULL){
        return
    }
    else{
        postorder(root->lchild)
        postorder(root->rchild)
        print(root->data)
    }
}



*/