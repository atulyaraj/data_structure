/*
Binary tree, non primitive, non linear.
It is a finite set of nodes such that 
    1. It has a Node having NULL value;
    2. Node called the root of a tree which has two disjoint sub trees 
        called the left sub tree and the right sub tree. 
    3. Where root is NULL it's an empty tree;

A is root
A is the parent/predecessor of B/C
Node B and C are children/sucessors of Node A
B is the left child
C is the right child
B and C are siblings because they have a common parent.
Node ABCD are called internal nodes as they have atleast one child.
Node GEF and called external/terminal nodes as they no child.


LEVEL
The level of the root node is 0.
The level of any node is one more than the level of it's parent.
B C at level 1
A 0
D E F 2
G 3

                            A
                        /       \
                       B         C
                    /       \       
                   D         E      
                 /
                F

HEIGHT/DEPTH
Number of nodes in the longest branch.
One more the highest level of the tree




Representation of binary tree:
Using array or Linked list

Array representation of binary tree:
A B C D E NULL F NULL G


Linked list representation:

struct Node{

    struct Node *lchild;
    int data;
    struct Node *rchild;
};


Initially:

    struct Node *root = NULL;

    Tree is empty.


Extended binary tree (2 tree):
Any binary tree can be extended to a 2-tree by inserting the extra nodesto the empty address fields. 
In a 2-tree all the internal nodes are represented by circles and the external nodes by rectangles.
Every parent has exactly 2 children.

Expression tree of the algebraic expression:

Create the tree and traverse it in pre, in, post order
a + (b * c) - d / e
l - m / n + (p * q) % r

    
*/

