/*
Application: Compressing of files

Huffman tree: an extended binary tree(2-tree) with the least total weights on it's edges.

Example on copy. 


Algorithm:

0. Start
1. Given n nodes with thier weights assigned to them. W1, W2, W3...
2. Choose two nodes with the least weights. eg: W1, W2 and Construct a 2-tree
3. Discard the nodes with the weights W1 and W2 and include the weight w = W1 + W2
4. Repeat 2 and 3 until the list is exhausted.
5. End


Construct a Huffman tree: 
Nodes:      A B C D E F
Weights:    3 2 5 4 3 6 


ABCAABCDAABCABBDE


*/