#ifndef ARRAY_H_
#define ARRAY_H_

extern int *array;

void array_menu();

void arrayData_structure();

void initialization();

void insertElements();

void viewArray();

void deleteElementMenu();

void deleteByIndex();

void deleteByValue();

void updateArray();

void searchElement();

void sortArray();

#endif