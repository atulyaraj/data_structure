#include"array.h"
#include<stdlib.h>
#include<stdio.h>

int *array;
int size = 0;

void arrayMenu(){
        printf("\t***********ARRAY************\t\n");
        printf("\t* a. Initialization\t   \n");
        printf("\t* b. Insert element\t   \n");
        printf("\t* c. Delete element\t   \n");
        printf("\t* d. Update element\t   \n");
        printf("\t* e. Sort Array\t\t   \n");
        printf("\t* f. Search element\t   \n");
        printf("\t* v. View Array\t   \n");
        printf("\t* 0. Close\t\t   \n");
        printf("\t****************************\n\n");
}

void initialization(){

        printf("\t *****Menu>Array>Initialization\n\n");
        printf("\t *Enter the size of array:");
        int sizeInput;
        scanf("%d", &sizeInput);


        array = (int *)malloc(sizeof(int) * sizeInput);
        size = sizeInput;

        printf("\t ****Array has been initialized!\n\n");
        printf("\t * r. Redo\n");
        printf("\t * n. No\n");
        char initOption;
        printf("\t * Array>Initialization>");
        scanf(" %c", &initOption);
        switch(initOption){
            case 'a': initialization();
                        break;
            case '0': break;
        }

}

void viewArray(){
    printf("\n\n\t ******* View Array*********\n");
    for(int i = 0 ; i < size; i++){
            printf("\t * Element %d: %d\n", i + 1, array[i] );
        }
        printf("\n\n");
    }

void insertElements(){

        for(int i = 0 ; i < size; i++){
            printf("\t *Enter the element %d: ", i + 1 );
            scanf("%d", &array[i]);
        }

        printf("\t * Elements inserted!!\n\n");
    }



    


    void deleteElementMenu(){
        char deleteOption;
        do{
        printf("\t ********* Delete Element*****\n\n");
        printf("\t *(a): By Index\n");
        printf("\t *(b): By Value\n");
        printf("\t *(0): Close\n\n");
        printf("\t * Array>Deletion>");
            scanf(" %c", &deleteOption);

            switch(deleteOption){
                case 'a': deleteByIndex(); break;
                case 'b': deleteByValue(); break;
                case '0': break;
                default: printf("Enter a valid option!!\n");                                
            }
        }while(deleteOption != '0');
    }


    void deleteByIndex(){

        printf("\t ***** Delete by Index****\n");
        printf("\t * Array>Deletion>By Index> Enter index: ");
        int index;
        scanf("%d", &index);

        for(int i = index - 1; i < size; i++){
            array[i] = array[i + 1];
        }

        viewArray();
    }


void deleteByValue(){
 printf("\t ***** Delete by Value****\n");
        printf("\t * Array>Deletion>By Value> Enter value: ");
        int value;
        scanf("%d", &value);

        for(int i = 0; i < size; i++){
            if(array[i] == value){
                printf("Value found\n\n");
            }
        }

        viewArray();
    }

        

void updateArray(){

}

 
void sortArray(){


}


void arrayData_structure(){
    arrayMenu();

    char input;

    do{
    printf("\t * Array>Input: ");
    scanf(" %c", &input);
    
     switch(input){
        case 'a': initialization(); break;
        case 'b': insertElements();  break;
        case 'c': deleteElementMenu(); break;
        case 'd': updateArray(); break;
        case 'f': sortArray(); break;
        case 'v': viewArray(); break;
        case '0':break;
        default: printf("Enter a valid option!!\n");                                

    }

    arrayMenu();


    }while(input != '0'); 
}