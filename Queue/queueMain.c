#include<stdio.h>
#include "queueMain.h"
#include "./SimpleQueue/queue.h"
#include "./CircularQueue/circularQueue.h"

// #include "./Deque/deque.h"
// #include "./InputRDeque/queue.h"
// #include "./OutputRDeque/queue.h"



void queueMainMenu(){

     printf("\t***********QUEUE***************\t\n");
        printf("\t* 1. Simple queue   \n");
        printf("\t* 2. Cicular queue  \n");
        printf("\t* 3. Deque   \n");
        printf("\t* 4. Input restricted deque   \n");
        printf("\t* 5. Output restricted deque   \n");
        printf("\t* 0. Close   \n");
        printf("\t****************************\n");
}

void queueMainDataStructure(){

    queueMainMenu();

    int input;

 do{
    printf("\t* Queue>Input: ");
    scanf("%d", &input);
    
     switch(input){
        case 1: queueDataStructure(); break;
        case 2: circularDataStructure();  break;
        case 3:  break;
        case 4:  break;
        case 5:  break;
        case 0:break;
        default: printf("Enter a valid option!!\n");                                
    }
    queueMainMenu();
    }while(input != '0'); 
}