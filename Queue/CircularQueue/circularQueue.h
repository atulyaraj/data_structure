#ifndef CIRCULARQUEUE_H_
#define CIRCULARQUEUE_H_

extern int *cQueue;

void circularMenu();

void circularDataStructure();

void initializationCQ();

void cEnqueue();

int cDequeue();

void viewCQueue();

void headCShow();

void tailCShow();

#endif