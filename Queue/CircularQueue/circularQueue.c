#include<stdio.h>
#include<stdlib.h>
#include"circularQueue.h"


int *cQueue;
int maxCQueue;
int headC, tailC;

void circularMenu(){

    printf("\t*******Circular Queue********\t\n");
    printf("\t* a. Initialization\t   \n");
    printf("\t* e. Enqueue\t   \n");
    printf("\t* d. Dequeue\t   \n");
    printf("\t* v. View queue\t \n");
    printf("\t* h. Head\t   \n");
    printf("\t* t. Tail\t   \n");
    printf("\t* 0. Close\t\t   \n");
    printf("\t****************************\n\n");

}

void circularDataStructure(){
    char input;
    circularMenu();
    do{
    printf("\t* Circular Queue>Input: ");
    scanf(" %c", &input);
    
     switch(input){
        case 'a': initializationCQ(); break;
        case 'e': cEnqueue();  break;
        case 'd': cDequeue(); break;
        case 'v': viewCQueue(); break;
        case 'h': headCShow(); break;
        case 't': tailCShow(); break;
        case '0':break;
        default: printf("\t* Enter a valid option!!\n");                                
    }
    circularMenu();
    }while(input != '0'); 


}

void initializationCQ(){

    printf("\t* Circular Queue>Initialization \t \n");
    printf("\t* Enter the size of queue: ");

    int sizeQueue;

    scanf("%d", &sizeQueue);

    maxCQueue = sizeQueue;

    cQueue = (int *)malloc(sizeof(int) * sizeQueue);

    headC = tailC = -1;

    printf("\t****Queue has been initialized!\n\n");
        printf("\t * r. Redo\n");
        printf("\t * n. No\n");
        char initOption;
        printf("\t *Circular Queue>Initialization> ");
        
        scanf(" %c", &initOption);
        switch(initOption){
            case 'a': initializationCQ();
                        break;
            case '0': break;
        }
}


void cEnqueue(){
    printf("\t* Circular Queue>Enqueue\n\n");

    if ((headC == 0 && tailC == maxCQueue - 1) ||
            headC == tailC + 1){
        printf("\t* Queue is Full\n\n");
        return;
    }
  
    else if (headC == -1) /* Insert First Element */{
        headC++;
    }
    
    tailC = (tailC + 1) % maxCQueue;

    printf("\t* Enter element: ");
    int data;

    scanf("%d", &data);

    cQueue[tailC] = data;
}

int cDequeue(){
    if (headC == -1){
        printf("\t* Queue is Empty\n\n");
        return -1;
    }
    else if (headC == tailC){
        // means only one element is there 
        int data = cQueue[headC];
        headC = tailC = -1;
        printf("\t* Dequeued: %d\n\n", data);

        return data;
    }else{

        int data = cQueue[headC];

        headC = (headC + 1) % maxCQueue;
        printf("\t* Dequeued: %d\n\n", data);
        return data;
    }
}

void viewCQueue(){

   if (headC == -1){
        printf("\t* Queue is Empty\n\n");
        return;
    }
    printf("\n\t *Elements in Circular Queue are: ");

    if (headC < tailC){
        for (int i = headC; i <= tailC; i++)
            printf("%d ",cQueue[i]);
        printf("\n\n");
    }
    else{
         for (int i = headC; i <= maxCQueue - 1; i++)
             printf("%d ", cQueue[i]);
  
         for (int i = 0; i <= tailC; i++)
             printf("%d ", cQueue[i]);

        printf("\n\n");
    }
}

void headCShow(){
    printf("\t* Head is at %d : %d\n\n",headC, cQueue[headC]);

}

void tailCShow(){
    printf("\t* Tail is at %d : %d\n\n",tailC, cQueue[tailC]);

}

