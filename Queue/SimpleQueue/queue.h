#ifndef QUEUE_H_
#define QUEUE_H_

extern int *queue;

void queueMenu();

void queueDataStructure();

void initializationQ();

void enqueue();

int dequeue();

void viewQueue();

void headElement();

void tailElement();

int queueStatus();

#endif