#include<stdio.h>
#include<stdlib.h>
#include"queue.h"

int *queue;
int maxQueue;
int head, tail;

void queueMenu(){
    printf("\t***********Queue************\t\n");
    printf("\t* a. Initialization\t   \n");
    printf("\t* e. Enqueue\t   \n");
    printf("\t* d. Dequeue\t   \n");
    printf("\t* v. View queue\t \n");
    printf("\t* h. Head\t   \n");
    printf("\t* t. Tail\t   \n");
    printf("\t* 0. Close\t\t   \n");
    printf("\t****************************\n\n");


}

void queueDataStructure(){

    char input;

    queueMenu();

    do{
    printf("\t* Queue>Input: ");
    scanf(" %c", &input);
    
     switch(input){
        case 'a': initializationQ(); break;
        case 'e': enqueue();  break;
        case 'd': dequeue(); break;
        case 'v': viewQueue(); break;
        case 'h': headElement(); break;
        case 't': tailElement(); break;
        case '0':break;
        default: printf("\t* Enter a valid option!!\n");                                
    }
    queueMenu();
    }while(input != '0'); 

}

void initializationQ(){

    printf("\t* Queue>Initialization \t \n");
    printf("\t* Enter the size of queue: ");

    int sizeQueue;

    scanf("%d", &sizeQueue);

    maxQueue = sizeQueue - 1;

    queue = (int *)malloc(sizeof(int) * sizeQueue);

    head = tail = -1;


    printf("\t****Queue has been initialized!\n\n");
        printf("\t * r. Redo\n");
        printf("\t * n. No\n");
        char initOption;
        printf("\t *Queue>Initialization> ");
        
        scanf(" %c", &initOption);
        switch(initOption){
            case 'a': initializationQ();
                        break;
            case '0': break;
        }


}

void enqueue(){

    printf("\t*Queue>Enqueue\n\n");
    printf("\t* Enter element: ");
    int data;
    scanf("%d", &data);

    if(queueStatus() == 0){

        head = 0;
        tail = 0;

    }else{

        if(tail >= maxQueue){
            printf("\t* Queue is full!!\n\n");
            return ;
        }

        tail++;
        
    }
    queue[tail] = data;
}

int dequeue(){

    int data = queue[head];

    head++;

    return data;
}

void viewQueue(){

    // for(int i = head; i <= tail; i++){
    //     printf("%d ", queue[i]);
    // }

    if(queueStatus() == 1){
        printf("\n\n\t* Queue View\n\n");

    for(int i = head; i <= tail; i++){
        printf("\t %d | ", queue[i]);
    }

    printf("\n\n");

    }else{
        printf("\n\n\t* Queue is empty!\n\n");
        return;
    }  

}

int queueStatus(){
    if(head == -1 && tail == -1)
        return 0;
    else
        return 1;
}

void headElement(){}

void tailElement(){}